# Map Coloring IDP

Project for the interactive coloring of a map of europe.
Connects to IDP-Z3 via a websocket.

Three functionalities are currently supported:
* Model expand: give each of the remaining countries a color, conform to the constraint.
* Propagate: given the current coloring, try to see if any other colors can be derived.
* "Guide": prevent errors from happening by automatically excluding infeasible colors (uses propagation).

Visualisation code from https://github.com/drfperez/europemap
