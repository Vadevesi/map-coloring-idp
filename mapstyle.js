var map = L.map('map', '', {
mousewheel: false
});


// Show map of EU
L.tileLayer(
"https://stamen-tiles.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}@2x.png"
).addTo(map);

map.scrollWheelZoom.disable();


function style(feature) {
    return {
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7,
        fillColor: '#808080'
    };
}

function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }

    // info.update(layer.feature.properties);
}

var geojson;

function resetHighlight(e) {
    geojson.resetStyle(e.target);
}

// Color in a country.
function setColor(layer) {
    if (layer.color === 'Red') {
      color = '#f7021a';
    } else if (layer.color == 'Blue') {
      color = '#0418ef';
    } else if (layer.color == 'Yellow') {
      color = '#f7da00';
    } else if (layer.color == 'Green') {
      color = '#18f700';
    } else {
      geojson.resetStyle(layer);
      return
    }
    layer.setStyle({
        weight: 2,
        color: color,
        fillColor: color,
        dashArray: '',
        fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }
}

function switchColor(e) {
    if (waiting) {
      return
    }
    var layer = e.target;

    lastClicked = layer.feature.properties['name'];

    if (layer.color === 'Red') {
      layer.color = 'Blue';
    } else if (layer.color == 'Blue') {
      layer.color = 'Yellow';
    } else if (layer.color == 'Yellow') {
      layer.color = 'Green';
    } else if (layer.color == 'Green') {
      layer.color = 'none';
      resetHighlight(e);
      return;
    } else {
      layer.color = 'Red';
    }

    if (layer.disallowedColors && layer.disallowedColors.includes(layer.color)) {
      // Switch once more if the color is not allowed.
      switchColor(e);
      return
    }
    setColor(layer);
    if (guideBox.checked) {
      propagate();
    }

}

function onEachFeature(feature, layer) {
    layer.on({
        //mouseover: highlightFeature,
        // mouseout: resetHighlight,
        click: switchColor
    });
}

geojson = L.geoJson(countriesData, {
    style: style,
    onEachFeature: onEachFeature
}).addTo(map);

map.attributionControl.addAttribution('&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap contributors</a>');
map.attributionControl.addAttribution('<a href="https://github.com/drfperez/europemap">Europemap by drfperez</a>.');

//storymap

var places = { type: 'FeatureCollection', features: [
{ geometry: { type: "Point", coordinates: [13.12960000, 52.50110000] },
properties: { id: "cover", zoom: 4 }, type: 'Feature' },
{ geometry: { type: "Point", coordinates: [31.15591514, 48.51830379] },
properties: { id: "ukraine", zoom: 6 }, type: 'Feature' },
{ geometry: { type: "Point", coordinates: [22.07571203, 47.51424049] },
properties: { id: "aldgate", zoom: 6 }, type: 'Feature' },
{ geometry: { type: "Point", coordinates: [3, 47] },
properties: { id: "london-bridge", zoom: 6 }, type: 'Feature' },
{ geometry: { type: "Point", coordinates: [-0.5, 54] },
properties: { id: "woolwich", zoom:6 }, type: 'Feature' },
{ geometry: { type: "Point", coordinates: [13.12960000, 52.50110000] },
properties: { id: "gloucester", zoom: 4 }, type: 'Feature' },
{ geometry: { type: "Point", coordinates: [-0.19684993, 51.5033856] },
properties: { id: "caulfield-gardens" }, type: 'Feature' },
{ geometry: { type: "Point", coordinates: [-0.10669358, 51.51433123] },
properties: { id: "telegraph" }, type: 'Feature' },
{ geometry: { type: "Point", coordinates: [-0.12416858, 51.50779757] },
properties: { id: "charing-cross" }, type: 'Feature' }
]};




var placesLayer = L.geoJson(places).addTo(map);

//set opacity of markers to 0
placesLayer.eachLayer(function(layer) {
layer.setOpacity(0);
});


//goodtohere

// Ahead of time, select the elements we'll need -
// the narrative container and the individual sections
var narrative = document.getElementById('narrative'),
sections = narrative.getElementsByTagName('section'),
currentId = '';

let mapWindow = document.getElementById('map')
let kbWindow = document.getElementById('kb')
let guideBox = document.getElementById('guide');
let showBox = document.getElementById('showCode');
let waiting = false;
let spinner = document.getElementById('spinner');
spinner.style.visibility = "hidden";
let explainField = document.getElementById('explain');
let explainBox = document.getElementById('explainBox');
let lastClicked = '';

// Set the listener code for showBox.
showBox.addEventListener('change', function() {
  if (this.checked) {
    mapWindow.style.visibility = 'hidden';
    kbWindow.style.visibility = 'visible';
  } else {
    mapWindow.style.visibility = 'visible';
    kbWindow.style.visibility = 'hidden';
  }

});

guideBox.addEventListener('click', function(event) {
  if (guideBox.checked) {
    propagate();
  }
});

setId('cover');

function setId(newId) {
// If the ID hasn't actually changed, don't do anything
if (newId === currentId) return;
// Otherwise, iterate through layers, setting the current
// marker to a different color and zooming to it.
placesLayer.eachLayer(function(layer) {
    if (layer.feature.properties.id === newId) {
        map.setView(layer.getLatLng(), layer.feature.properties.zoom || 14);   
        
    } else {
        
    }
});
// highlight the current section
for (var i = 0; i < sections.length; i++) {
    sections[i].className = sections[i].id === newId ? 'active' : '';
}
// And then set the new id as the current one,
// so that we know to do nothing at the beginning
// of this function if it hasn't changed between calls
currentId = newId;
}

// If you were to do this for real, you would want to use
// something like underscore's _.debounce function to prevent this
// call from firing constantly.
narrative.onscroll = function(e) {
var narrativeHeight = narrative.offsetHeight;
var newId = currentId;
// Find the section that's currently scrolled-to.
// We iterate backwards here so that we find the topmost one.
for (var i = sections.length - 1; i >= 0; i--) {
    var rect = sections[i].getBoundingClientRect();
    if (rect.top >= 0 && rect.top <= narrativeHeight) {
        newId = sections[i].id;
    }
};
setId(newId);
};



function colorCountries(colors) {
  geojson.eachLayer(function(layer) {
    let name = layer.feature.properties.name;
    if (name === 'Bosnia and Herz.') {
        name = 'Bosnia_and_Herzegovina';
    } else if (name === 'Czech Rep.') {
        name = 'Czechia';
    } else if (name === 'United Kingdom') {
        name = 'UK';
    }
    if (colors.hasOwnProperty(name)) {
      layer.color = colors[name]
      setColor(layer);
    } else {
      return
    }
  })

}

function removeColors(disallowedColors) {
  geojson.eachLayer(function(layer) {
    let name = layer.feature.properties.name;

    if (name === lastClicked) {
      return;
    }

    if (name === 'Bosnia and Herz.') {
        name = 'Bosnia_and_Herzegovina';
    } else if (name === 'Czech Rep.') {
        name = 'Czechia';
    } else if (name === 'United Kingdom') {
        name = 'UK';
    }
    if (disallowedColors.hasOwnProperty(name)) {
      layer.disallowedColors = disallowedColors[name];
    } else {
      return;
    }
  })
}

function generateColorsDict() {
  let colors = {}
  geojson.eachLayer(function(layer) {
    if (layer.hasOwnProperty('color') && layer.color !== 'none') {
      let name = layer.feature.properties.name;
      if (name === 'Bosnia and Herz.') {
          name = 'Bosnia_and_Herzegovina';
      } else if (name === 'Czech Rep.') {
          name = 'Czechia';
      } else if (name === 'United Kingdom') {
          name = 'UK';
      }
      colors[name] = layer.color
    }
  });
  return colors;
}


var theory = `
vocabulary {
    type Country := {Albania, Austria, Belarus, Belgium, Bosnia_and_Herzegovina, Bulgaria, Croatia, Czechia, Denmark, Estonia, Finland, France, Germany, Greece, Hungary, Iceland, Ireland, Italy, Kosovo, Latvia, Lithuania, Luxembourg, Netherlands, Norway, Macedonia, Montenegro, Moldova, Poland, Portugal, Romania, Russia, Serbia, Slovakia, Slovenia, Spain, Sweden, Switzerland, Ukraine, UK}
  type Color := {Red, Blue, Green, Yellow}
  Bordering : (Country * Country) -> Bool
  ColorOf : (Country) -> Color
}


theory {
  [No countries can share the same color]
  (!c1, c2 in Country: Bordering(c1, c2) => ColorOf(c1) ~= ColorOf(c2)).
}

structure {
    Bordering := {(Albania, Greece), (Albania, Macedonia), (Albania, Montenegro), (Albania, Kosovo), (Austria, Slovenia), (Austria, Italy), (Austria, Switzerland), (Austria, Germany), (Austria, Czechia), (Austria, Slovakia), (Austria, Hungary), (Belarus, Ukraine), (Belarus, Poland), (Belarus, Lithuania), (Belarus, Latvia), (Belarus, Russia), (Belgium, Netherlands), (Belgium, Germany), (Belgium, Luxembourg), (Belgium, France), (Bosnia_and_Herzegovina, Croatia), (Bosnia_and_Herzegovina, Serbia), (Bosnia_and_Herzegovina, Montenegro), (Bulgaria, Romania), (Bulgaria, Greece), (Bulgaria, Macedonia), (Bulgaria, Serbia), (Croatia, Slovenia), (Croatia, Hungary), (Croatia, Serbia), (Croatia, Bosnia_and_Herzegovina), (Czechia, Poland), (Czechia, Slovakia), (Czechia, Austria), (Czechia, Germany), (Denmark, Germany), (Estonia, Russia), (Estonia, Latvia), (Finland, Norway), (Finland, Sweden), (Finland, Russia), (France, Belgium), (France, Luxembourg), (France, Germany), (France, Switzerland), (France, Italy), (France, Spain), (Germany, Denmark), (Germany, Poland), (Germany, Czechia), (Germany, Austria), (Germany, Switzerland), (Germany, France), (Germany, Luxembourg), (Germany, Belgium), (Germany, Netherlands), (Greece, Albania), (Greece, Macedonia), (Greece, Bulgaria), (Hungary, Slovakia), (Hungary, Ukraine), (Hungary, Romania), (Hungary, Serbia), (Hungary, Croatia), (Hungary, Slovenia), (Hungary, Austria), (Ireland, UK), (Italy, France), (Italy, Switzerland), (Italy, Austria), (Italy, Slovenia), (Kosovo, Serbia), (Kosovo, Macedonia), (Kosovo, Albania), (Kosovo, Montenegro), (Latvia, Estonia), (Latvia, Russia), (Latvia, Belarus), (Latvia, Lithuania), (Lithuania, Latvia), (Lithuania, Belarus), (Lithuania, Poland), (Lithuania, Russia), (Luxembourg, Belgium), (Luxembourg, Germany), (Luxembourg, France), (Netherlands, Germany), (Netherlands, Belgium), (Norway, Sweden), (Norway, Finland), (Macedonia, Kosovo), (Macedonia, Serbia), (Macedonia, Bulgaria), (Macedonia, Greece), (Macedonia, Albania), (Montenegro, Croatia), (Montenegro, Bosnia_and_Herzegovina), (Montenegro, Serbia), (Montenegro, Kosovo), (Montenegro, Albania), (Poland, Russia), (Poland, Lithuania), (Poland, Belarus), (Poland, Ukraine), (Poland, Slovakia), (Poland, Czechia), (Poland, Germany), (Portugal, Spain), (Romania, Ukraine), (Romania, Moldova), (Romania, Bulgaria), (Romania, Serbia), (Romania, Hungary), (Russia, Norway), (Russia, Finland), (Russia, Estonia), (Russia, Latvia), (Russia, Belarus), (Russia, Ukraine), (Serbia, Hungary), (Serbia, Romania), (Serbia, Bulgaria), (Serbia, Macedonia), (Serbia, Kosovo), (Serbia, Montenegro), (Serbia, Bosnia_and_Herzegovina), (Serbia, Croatia), (Slovakia, Poland), (Slovakia, Ukraine), (Slovakia, Hungary), (Slovakia, Austria), (Slovakia, Czechia), (Slovenia, Austria), (Slovenia, Hungary), (Slovenia, Croatia), (Slovenia, Italy), (Spain, Portugal), (Spain, France), (Sweden, Norway), (Sweden, Finland), (Switzerland, Germany), (Switzerland, Austria), (Switzerland, Italy), (Switzerland, France), (Ukraine, Belarus), (Ukraine, Moldova), (Ukraine, Russia), (Ukraine, Hungary), (Ukraine, Slovakia), (Ukraine, Poland), (UK, Ireland)}.
}
`
kbWindow.innerText = theory;

var socket;
function initKb() {
  socket.send(JSON.stringify({'method': 'create',
                          'theory': theory}));
}

function initSocket() {
	loading(true);
  //socket = new WebSocket("ws://localhost:8765");
  //socket = new WebSocket("ws://reasoning-websocket.cs.kuleuven.be:8765");
  socket = new WebSocket("wss://websocket.simonvandevelde.be");
  //if (navigator.userAgent.indexOf("Chrome") != -1) {
  //  // Use WSS for Chrome.
  //  socket = new WebSocket("wss://idpsocket.herokuapp.com/");
  //} else {
  //  // Use WS else. FF complains about HerokuApp's cert not being valid.
  //  // Should be fixed once we have proper hosting.
  //  socket = new WebSocket("ws://idpsocket.herokuapp.com/");

  //}
  socket.onopen = function(e) {
    initKb();
  }
  socket.onmessage = function(event) {
    explainField.innerHTML = '';
    data = JSON.parse(event.data);
    if (data['method'] === 'modelexpand') {
      if (data['success']) {
        colorCountries(data['models'][0]['ColorOf']);
	   	  loading(false);
      } else {
        explain();
      }
    } else if (data['method'] === 'propagate') {
      if (data['success']) {
        colorCountries(data['posdict']['ColorOf']);
        // TODO: add if-statement
        removeColors(data['negdict']['ColorOf']);
	   	  loading(false);
      } else {
        explain();
      }
    } else if (data['method'] === 'explain') {
      explanation = 'Explanation:<br>'
      explanation += data['explanation'].replace(/\n/g,'<br>')
      console.log(explanation);
      explainField.innerHTML = explanation
      loading(false);
      console.log(data);
    } else if (data['method'] === 'create') {
      if (data['success']) {
        loading(false);
      } else {
        loading(false);
        narrative.innerHTML = 'Server error. <br>' + narrative.innerHTML;
      }
    } else {
      console.log(data);
    }
  }
}

function loading(bool) {
  if (bool) {
		waiting = true;
		spinner.style.visibility = "visible";
	} else {
		waiting = false;
		spinner.style.visibility = "hidden";
	}
}

function modelExpand() {
  // Hide explain pane
  explainBox.style.visibility = 'hidden';
     
  loading(true);
  enumeration = generateColorsDict()
  msg = {'method': 'setenumeration',
         'symbol': 'ColorOf',
         'enum': enumeration}
  socket.send(JSON.stringify(msg))
  msg = {'method': 'modelexpand',
         'number': 1};
  socket.send(JSON.stringify(msg));

}

function propagate() {
  // Hide explain pane
  explainBox.style.visibility = 'hidden';

  loading(true);
  enumeration = generateColorsDict()
  msg = {'method': 'setenumeration',
         'symbol': 'ColorOf',
         'enum': enumeration}
  socket.send(JSON.stringify(msg))
  msg = {'method': 'propagate',
         'symbol': 'ColorOf'}
  socket.send(JSON.stringify(msg));
}

function reset() {
  // Hide explain pane
  explainBox.style.visibility = 'hidden';

  geojson.eachLayer(function(layer) {
    layer.color = 'none';
    layer.disallowedColors = [];
    geojson.resetStyle(layer);
  })
}
  

function explain() {
  loading(true);
  explainBox.style.visibility = 'visible';
  explainField.innerHTML = 'Error found, preparing explanation';
  enumeration = generateColorsDict()
  msg = {'method': 'explain',
         'number': 3};
  socket.send(JSON.stringify(msg));
}

initSocket()

